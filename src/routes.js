import React from 'react';  
import { Route, IndexRoute } from 'react-router';  
import App from './components/App';  
import HomePage from './components/home/HomePage';  
import StatesPage from './components/states/StatesPage';  
// import CatPage from './components/cats/CatPage';

export default (  
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />
    <Route path="/states" component={StatesPage} />
  </Route>
);