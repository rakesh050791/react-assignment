import {combineReducers} from 'redux';  
import states from './stateReducer';

const rootReducer = combineReducers({  
  // short hand property names
  states
})

export default rootReducer;  