import React, {PropTypes} from 'react';
import {Link, browserHistory} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import StateList from './StateList';
import * as actions from '../../actions/stateActions'

class StatesPage extends React.Component {
  componentWillMount() {
    if (this.props.states[0].id == '') {
      this.props.actions.loadStates();
    }
  }
  render() {
    const states = this.props.states;
    return (
      <div className="col-md-12">
        {/* <h1>States <Link to={'/cats/new'} className="btn btn-primary">+ cat</Link></h1> */}
        <div className="col-md-4">
          <StateList states={states} />
        </div>
        <div className="col-md-8">
          {this.props.children}
        </div>
      </div>
    );
  }
}

StatesPage.propTypes = {
  states: PropTypes.array.isRequired,
  children: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  if (state.states.length > 0) {
    return {
     states: state.states
    };
  } else {
    return {
        states: [{id: '', name: '', code: ''}]
    }
  }
}

function mapDispatchToProps(dispatch) {
  return {actions: bindActionCreators(actions, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(StatesPage);