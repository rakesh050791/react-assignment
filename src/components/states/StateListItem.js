import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import StatePage from './StatePage';

const StateListItem = ({state}) => {
  return (
    <li className="list-group-item"><Link to={'/states/' + state.id}>{state.name}</Link></li>
  );
};

StateListItem.propTypes = {
    state: PropTypes.object.isRequired
};

export default StateListItem;