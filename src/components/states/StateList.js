import React, {PropTypes} from 'react';
import StateListItem from './StateListItem';
import {Link} from 'react-router';


const StateList = ({states}) => {
  return (
      <ul className="list-group">
        {states.map(state => 
           <li className="list-group-item" key={state.id}><Link to={'/cats/' + state.id}>{state.name}</Link></li>
        )}
      </ul>
  );
};

StateList.propTypes = {
  states: PropTypes.array.isRequired
};

export default StateList;