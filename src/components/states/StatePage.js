import React, {PropTypes} from 'react';  
import {connect} from 'react-redux';  
import * as stateActions from '../../actions/stateActions';

class StatePage extends React.Component {
    constructor(props, context) {
      super(props, context);
      this.state = {
        state: this.props.state
      };
      this.redirect = this.redirect.bind(this);
    }
    
  
  
    componentWillReceiveProps(nextProps) {
      if (this.props.state.id != nextProps.state.id) {
        this.setState({state: nextProps.state});
      }
    //   this.setState({saving: false, isEditing: false});
    }
}


// StatePage.propTypes = {
//     state: PropTypes.object.isRequired,
//     actions: PropTypes.object.isRequired
// };

  
  function getStateById(states, id) {
    let state = states.find(state => state.id == id)
    return Object.assign({}, state)
  }

  function mapStateToProps(state, ownProps) {
    // const stateHobbies = Object.assign([], state.hobbies)
    // let checkBoxHobbies = [];
    // let catHobbies = [];
    let cat = {name: '', code: ''};
    const stateId = ownProps.params.id;
    // if (catId && state.cats.length > 0) {
    //   state = getStateById(state.states, ownProps.params.id);
    //   if (cat.id) {
    //     checkBoxHobbies = hobbiesForCheckBoxes(stateHobbies, cat);
    //     catHobbies = collectCatHobbies(stateHobbies, cat);
    //   } else {
    //     checkBoxHobbies = hobbiesForCheckBoxes(stateHobbies)
    //   }
    // } 
      return {cat: cat};
  }

export default connect(mapStateToProps)(StatePage);  
