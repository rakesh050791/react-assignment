import React from 'react';
import {Link} from 'react-router';

class HomePage extends React.Component {
  render() {
    return (
      <div className="jumbotron">
        <h1>State Book</h1>
        <p>the best way manage your state collection.</p>
      </div>
    );
  }
}

export default HomePage;