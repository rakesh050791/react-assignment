import * as types from './actionTypes';  
import stateApi from '../api/stateApi';

export function loadStates() {  
  return function(dispatch) {
    return stateApi.getAllStates().then(states => {
      dispatch(loadStatesSuccess(states));
    }).catch(error => {
      throw(error);
    });
  };
}

export function loadStatesSuccess(states) {  
    return {type: 'LOAD_STATES_SUCCESS', states};
  }